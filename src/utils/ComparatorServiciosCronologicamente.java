package utils;

import java.util.Comparator;

import model.vo.Servicio;

public class ComparatorServiciosCronologicamente implements Comparator<Servicio>{
	
	/**
	 * Método que compara dos servicios cronológicamente.
	 */
	public int compare(Servicio s1, Servicio s2)
	{
		int oli;
		
		if((oli = s1.getTrip_start_timestamp().compareTo(s2.getTrip_start_timestamp())) != 0)
		{
			return oli;
		}
		else
		{
			return s1.getTrip_end_timestamp().compareTo(s2.getTrip_end_timestamp());
		}
	}
}
