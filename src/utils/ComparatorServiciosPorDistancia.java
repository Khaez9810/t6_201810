package utils;

import java.util.Comparator;

import model.vo.Servicio;

public class ComparatorServiciosPorDistancia implements Comparator<Servicio>{

	/**
	 * M�todo que compara dos servicios por la distancia que se recorri� en ellos.
	 */
	public int compare(Servicio s1, Servicio s2)
	{
		return s1.getTripMiles().compareTo(s1.getTripMiles());
	}
}
