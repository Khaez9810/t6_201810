package test;
import junit.framework.*;
import model.data_structures.Queue;


public class QueueTest extends TestCase{

	private Queue<Integer> Cola;

	private void setupEscenario1() {
		Cola = new Queue<Integer>();
	}

	public void testQueue() {
		setupEscenario1();
		assertTrue("No se inicializo la pila correctamente", Cola.isEmpty());
	}

	public void testAll(){
		try {

			setupEscenario1();
			
			Cola.enqueue(1);
			Cola.enqueue(2);
			Cola.enqueue(3);
			Cola.enqueue(4);
			Cola.enqueue(15);
			Cola.enqueue(30);
			Cola.enqueue(5);
 
			assertNotNull("No se esta agregando correctamente",Cola.getHead());
			assertNotNull("No se esta agregando correctamente",Cola.getTail());
			assertSame("No se tiene la cola correcta",5 ,Cola.getTail().getValue());
			assertSame("El tamanio no es el correcto",7,Cola.getSize());
			assertSame("No se esta eliminando correctamete ",1, Cola.dequeue());
			assertSame("No se tiene la cabeza correcta",2 ,Cola.getHead().getValue());
			assertSame("No se esta eliminando correctamete ",2, Cola.dequeue());
			assertSame("No se tiene la cabeza correcta",3 ,Cola.getHead().getValue());


			

		} catch (Exception e) {
			System.out.println(e);
			assertFalse(e.getMessage(),e!=null);
		}
	}


}
