package view;

import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Servicio;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

			case 2: //Requerimiento 1

				


				System.out.println("-- �Qu� �rea deseas consultar?");
				int area = sc.nextInt();

				try
				{
					LinkedList<Servicio> rta = Controller.serviciosPorArea(area);
					int sais = rta.size();
					for (int i = 0; i < sais; i++) {
						System.out.println(rta.removeFirst());
					}

					break;
				}
				catch(NullPointerException nul)
				{
					System.out.println("-- No existe ning�n servicio que haya empezado en esa �rea.");
				}
				break;

			case 3: //Requerimiento 2

				System.out.println("-- �Los servicios con qu� distancia deseas consultar?");
				double distancia = sc.nextDouble();

				try
				{
					Iterator<Servicio> iterReq2 = Controller.serviciosPorDistancia(distancia).iterator();
					System.out.println("-- Se revisar�n los servicios con distancia dentro del intervalo " + 
							(distancia != 0 ? (Math.ceil(distancia/10) - 9) : 0)*10 + " - " + (Math.ceil(distancia/10)*10));
					Servicio s;
					while(iterReq2.hasNext())
					{
						s = iterReq2.next();
						System.out.println("-- " + s.toString() + ". La distancia que recorri� fue: " + s.getTripMiles());
					}
					break;
				}
				catch(NullPointerException nul)
				{
					System.out.println("-- No existe ning�n servicio que est� dentro del rango de esa distancia.");
				}
				break;
			case 4: //Caso salida

				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------TALLER6----------------------");
		System.out.println("\nCargar data:");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");
		System.out.println("Requerimiento1:");
		System.out.println("2. Obtenga una lista con todos los servicios de taxi ordenados cronologicamente por su fecha/hora inicial, \n"
				+ "que se prestaron en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. (1A)"
				+ "\nPd: Solo carga el primero. ");
		System.out.println("Requerimiento2:");
		System.out.println("3. Obtenga el numero de companias que tienen al menos un taxi inscrito y el numero total de taxis que trabajan para al menos una compania.\n"
				+ "Luego, genera una lista (en orden alfabetico) de las companias a las que estan inscritos los servicios de taxi. Para cada una, indique su nombre y el numero de taxis que tiene registrados. (1B)");
		System.out.println("4. Salir");
		System.out.println("\nType the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
