package controller;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Servicio;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	
	public static LinkedList<Servicio> serviciosPorArea(int area)
	{
		return manager.serviciosPorArea(area);
	}
	
	public static LinkedList<Servicio> serviciosPorDistancia(double distancia)
	{
		return manager.serviciosPorDistancia(distancia);
	}
}