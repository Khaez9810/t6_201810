package api;

import model.data_structures.LinkedList;
import model.vo.Servicio;

/**
 * API para la clase de logica principal  
 */
public interface ITaxiTripsManager 
{
	public boolean cargarSistema(String direccionJson);
	
	public LinkedList<Servicio> serviciosPorArea(int area);
	
	public LinkedList<Servicio> serviciosPorDistancia(double distancia);
}
