package model.data_structures;

import com.sun.org.apache.xml.internal.serializer.ElemDesc;

public class ArrayList<T> {

	private T[] array;
	private int size;

	public ArrayList(int capacidad) {
		this.size = 0;
		array = (T[]) new Object[1];
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size==0;
	}

	public boolean add(T item) {
		if(size==array.length) {
			T[] newarray = (T[]) new Object[this.size*4];
			for (int i = 0; i < array.length; i++) {
				newarray[i] = array[i];
			}
			array = newarray;
		}
		array[size++] = item;
		return true;
	}
	
	

	public boolean remove(T object) {
		for (int i = 0; i < array.length; i++) {
			if(array[i].equals(object)) {
				this.remove(i);
				return true;
			}
		}
		return false;
	}

	public T remove(int index) {
		T remove = array[index];
		T[] newarray = (T[]) new Object[this.size-1];
		for (int i = 0; i < array.length; i++) {
			if(!array[i].equals(remove)) newarray[i] = array[i];
		}
		size--;
		return remove;
	}
	
	public void clear() {
		size=0;
		array = (T[]) new Object[1];
	}
	
	public T get(int index) {
		if(index<size) return array[index];
		else return null;
	}
	
	public int indexOf(T item) {
		if(item!=null) {
			int i=0; boolean f=false;
			for (; i < array.length && !f; i++) {
				if(array[i].equals(item)) f=true;
			}
			return f?i:-1;
		}else return -1;
	}
	
	public T set(T item, int index) {
		if(index<size) {
			T oldItem = array[index];
			array[index] = item;
		}return null;
	}

}
