package model.data_structures;

public class TableNode <K,V> {
	
	private K key;
	private V value;
	private TableNode<K, V> next;
	
	public TableNode(K key,V value){
		this.key = key;
		this.value = value;
		next=null;
	}
	
	public K getKey(){
		return key;
	}
	
	public V getValue(){
		return value;
	}
	
	public void setKey(K key){
		this.key = key;
	}
	
	public V setValue(V value){
		V old = this.value;
		this.value = value;
		return old;
	}
	
	public void setNext(TableNode<K,V> next) {
		this.next = next;
	}

	public TableNode<K, V> getNext(){
		return this.next;
	}

}
