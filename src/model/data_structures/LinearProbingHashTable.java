package model.data_structures;

import java.util.Iterator;

public class LinearProbingHashTable<K,V> implements IHashTable<K, V>{

	private final static double FACTOR = 0.5;
	private int size;
	private int capacity;
	private TableNode<K, V>[] casillas;

	
	//Constructor
	public LinearProbingHashTable(int capacity){
		size = 0;
		this.capacity = capacity;
		casillas = (TableNode<K, V>[])new TableNode[this.capacity];
	}

	//Hashea la llave para ubicarla dentro del arreglo
	public int hashIndex(K key) {
		if(key==null) return -1;
		else{
			int  indexKey=(key.hashCode()&0x7fffffff)%size;
			return indexKey;
		}
	}

	//Ubica el nodo dentro del arreglo
	public void put(K key, V value){
		double factoractual = (double)size/(double)capacity;
		if(key!=null) {
			if(factoractual<FACTOR) reHash();
			int hashedKey= hashIndex(key);
			int index = findSlot(hashedKey, key);
			if(index>=0){
				casillas[index] = new TableNode<K,V>(key, value);
				size++;
				return;
			}
		}
	}

	//Se obtiene un valor del arreglo segun una llave
	public V get(K key){
		int index = findSlot(hashIndex(key), key);
		if(index<0) return null;
		return casillas[index].getValue();
	}

	//Se borra un valor del arreglo segun una llave
	public V delete(K key){
		int index = findSlot(hashIndex(key),key);
		if(index<0) return null;
		V ans = casillas[index].getValue();
		casillas[index] = new TableNode<K,V>(null,null);
		return ans;
	}

	public Iterator<K> keys(){
		LinkedList<K> llaves = new LinkedList<K>();
		for (TableNode<K, V> tableNode : casillas) {
			llaves.add(tableNode.getKey());
		}
		return llaves.iterator();
	}

	//Rehash y resize de la tabla
	private void reHash(){
		TableNode<K,V>[] old = casillas;
		capacity *=2;
		casillas = (TableNode<K,V>[]) new TableNode[capacity];
		for (int i = 0; i < old.length; i++) {
			if(old[i]!=null) {
				//En el put se rehashean las llaves
				this.put(old[i].getKey(), old[i].getValue());
			}
			
		}
	}
	
	
	//Busca una posicion en el arreglo disponible para la llave
	private int findSlot(int h, K k) {
	    int avail = -1;                               
	    int j = h;                                    
	    do {
	      if (isFull(j)) {                       	  
	        if (avail == -1) avail = j;               
	        if (casillas[j] == null) break;              
	      } else if (casillas[j].getKey().equals(k))
	        return j;                                 
	      j = (j+1) % capacity;                       
	    } while (j != h);                             
	    return -(avail + 1);                       
	  }
	

	private boolean isFull(int i){
		TableNode<K,V> ask = casillas[i];
		return ask==null || ask.getValue()==null;
	}
		
}
