package model.data_structures;

public class Queue<T extends Comparable<T>> implements IQueue<T> {

	
	private LinkedList<T> List;

	 public Queue() {
		 List = new LinkedList<T>();
		// TODO Auto-generated constructor stub
	}
	
	
	public Node<T> getHead() {
		return List.getHead();
	}


	public Node<T> getTail() {
		return List.getTail();
	}


	public int getSize() {
		return List.getSize();
	}


	@Override
	public void enqueue(T item) throws Exception {
		if(!List.add(item)) throw new Exception("No se agrego el elemento");
		}
		// TODO Auto-generated method stub

	@Override
	public T dequeue(){
		T aux  = getHead().getValue();
		if(List.deleteAtHead()) return aux; else return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return List.getSize()==0;
	}

}
