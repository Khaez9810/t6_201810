package model.logic;

import api.ITaxiTripsManager;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Date;
import com.google.gson.*;

import model.data_structures.IList;

import model.data_structures.LinkedList;
import model.data_structures.LinkedListHash;

import model.data_structures.SeparateChaningHasTable;

import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;
import utils.ComparatorServiciosCronologicamente;
import utils.ComparatorServiciosPorDistancia;

public class TaxiTripsManager implements ITaxiTripsManager 
{

	public static final ComparatorServiciosPorDistancia COMPARATOR_SERVICIOS_DISTANCIA = new ComparatorServiciosPorDistancia();

	public static final ComparatorServiciosCronologicamente COMPARATOR_SERVICIOS_CRONOLOGICAMENTE = new ComparatorServiciosCronologicamente();

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-08-02-2017.json";
	public static final String NO_S_DATA ="N/A";
	private boolean big = false;

	// Atributos

	/**
	 * Hash table de los servicios de cada �rea.
	 */
	private SeparateChaningHasTable<Integer,Servicio> serviciosPorArea;

	/**
	 * Hash table de los servicios seg�n sus distancias recorridas.
	 */
	private SeparateChaningHasTable<Double,Servicio> serviciosPorDistancia;


	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		if(direccionJson.equals(DIRECCION_LARGE_JSON)){cargarBig();//println(DIRECCION_LARGE_JSON);}
		if(!big){
			//Se declaran las estructuras en caso de no usar el archivo grande, o que se este cargando un archivo diferente al primero
			serviciosPorArea = new SeparateChaningHasTable(10);
			serviciosPorDistancia = new SeparateChaningHasTable(10);
		}
		JsonParser parser = new JsonParser();

		//Strings necesarios para la carga

		String company="",dropoff_census_track="",dropoff_centroid_latitude="",dropoff_centroid_longitude="",
				dropoff_community_area="", payment_type="", pickup_census_track="",
				pickup_centroid_latitude="",pickup_centroid_longitude="",pickup_community_area="",
				taxi_id="",trip_end_timestamp="",trip_id="",trip_start_timestamp="",extras="",fares="",tips="",tolls="",dropoff_type="",pickup_type="";
		int trip_seconds=0;
		double trip_miles=0, trip_total=0;
		JsonArray pickup_centroid_location=null,dropoff_centroid_location=null;
		Servicio servicio;

		int p = 0;
		try {
			JsonArray array = (JsonArray) parser.parse(new FileReader(direccionJson));
			for (int i = 0; i < array.size(); i++) {
				JsonObject ob = (JsonObject)array.get(i);

				//Se cargaron todos los datos para verificar que el lector este funcionando correctamente
				//Nota: Cambiar a tipo de datos correspontiendes

				company							=(ob.get("company")!=null)?ob.get("company").getAsString():NO_S_DATA;
				dropoff_census_track			=(ob.get("dropoff_census_tract")!=null)?ob.get("dropoff_census_tract").getAsString():NO_S_DATA;
				dropoff_centroid_latitude		=(ob.get("dropoff_centroid_latitude")!=null)?ob.get("dropoff_centroid_latitude").getAsString():NO_S_DATA;
				dropoff_centroid_longitude		=(ob.get("dropoff_centroid_longitude")!=null)?ob.get("dropoff_centroid_longitude").getAsString():NO_S_DATA;
				//dropoff_centroid_location 	=(ob.get("dropoff_centroid_location")!=null)?ob.get("dropoff_centroid_location").getAsJsonArray():null;
				dropoff_community_area			=(ob.get("dropoff_community_area")!=null)?ob.get("dropoff_community_area").getAsString():"0";
				extras							=(ob.get("extras")!=null)?ob.get("extras").getAsString():NO_S_DATA;
				fares							=(ob.get("fares")!=null)?ob.get("fares").getAsString():NO_S_DATA;
				payment_type					=(ob.get("payment_type")!=null)?ob.get("payment_type").getAsString():NO_S_DATA;
				pickup_census_track				=(ob.get("pickup_census_track")!=null)?ob.get("pickup_census_track").getAsString():NO_S_DATA;
				pickup_centroid_latitude		=(ob.get("pickup_centroid_latitude")!=null)?ob.get("pickup_centroid_latitude").getAsString():NO_S_DATA;
				pickup_centroid_longitude		=(ob.get("pickup_centroid_longitude")!=null)?ob.get("pickup_centroid_longitude").getAsString():NO_S_DATA;
				//pickup_centroid_location 		=(ob.get("pickup_centroid_location")!=null)?ob.get("pickup_centroid_location").getAsJsonArray():null;
				pickup_community_area			=(ob.get("pickup_community_area")!=null)?ob.get("pickup_community_area").getAsString():"0";
				taxi_id							=(ob.get("taxi_id")!=null)?ob.get("taxi_id").getAsString():NO_S_DATA;
				tips							=(ob.get("tips")!=null)?ob.get("tips").getAsString():NO_S_DATA;
				tolls							=(ob.get("tolls")!=null)?ob.get("tolls").getAsString():NO_S_DATA;
				trip_end_timestamp				=(ob.get("trip_end_timestamp")!=null)?ob.get("trip_end_timestamp").getAsString():NO_S_DATA;
				trip_id							=(ob.get("trip_id")!=null)?ob.get("trip_id").getAsString():NO_S_DATA;
				trip_miles						=(ob.get("trip_miles")!=null)?ob.get("trip_miles").getAsDouble():0;
				trip_seconds					=(ob.get("trip_seconds")!=null)?ob.get("trip_seconds").getAsInt():0;
				trip_start_timestamp			=(ob.get("trip_start_timestamp")!=null)?ob.get("trip_start_timestamp").getAsString():NO_S_DATA;
				trip_total						=(ob.get("trip_total")!=null)?ob.get("trip_total").getAsDouble():0;
				/*for (int j = 0; j < dropoff_centroid_location.size(); j++) {
					JsonObject data = (JsonObject) dropoff_centroid_location.get(j);
					dropoff_type 			=(data.get("type")!=null)?data.get("type").getAsString():NO_S_DATA;
				}
				for (int j = 0; j < pickup_centroid_location.size(); j++) {
					JsonObject data = (JsonObject) pickup_centroid_location.get(j);
					pickup_type 			=(data.get("type")!=null)?data.get("type").getAsString():NO_S_DATA;
				}
				 */
				servicio = new Servicio(trip_id,DateParserString(trip_start_timestamp),DateParserString(trip_end_timestamp),trip_seconds,trip_miles,dropoff_community_area);

				if(trip_id!=NO_S_DATA)
				{
					serviciosPorArea.put(Integer.parseInt(pickup_community_area),servicio);
				}
				if(pickup_community_area!=null)
				{
					serviciosPorDistancia.put(trip_miles, servicio);
				}


				p++;

				//				//println("================================================================================");
				//				//println("Compania:" + company);
				//				//println("Id Zona:" + dropoff_community_area);
				//				//println("trip id:" + trip_id);
				//				//println("================================================================================");

			}		
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//		serviciosPorArea.sort(COMPARATOR_SERVICIOS_CRONOLOGICAMENTE);
		//		serviciosPorDistancia.sort(COMPARATOR_SERVICIOS_DISTANCIA);
		//println(p);
		}
		return true;
	}

	/**
	 * M�todo que carga los servicios que inician en un �rea en orden cronol�gico.
	 */
	public LinkedList<Servicio> serviciosPorArea(int area)
	{
		LinkedList<Servicio> lista = serviciosPorArea.getAll(area); 
		//lista.sort(COMPARATOR_SERVICIOS_CRONOLOGICAMENTE);
		return lista;
	}

	/**
	 * M�todo que carga los servicios seg�n las millas que recorri�.
	 */
	public LinkedList<Servicio> serviciosPorDistancia(double distancia)
	{
		LinkedList rta = serviciosPorDistancia.getAll(distancia != 0 ? Math.ceil(distancia/10) : 1);
		//rta.sort(COMPARATOR_SERVICIOS_DISTANCIA);
		return rta;
	}

	private void cargarBig(){
		for (int i = 2; i < 8; i++) {
			String base = "./data/taxi-trips-wrvz-psew-subset-0"+i+"-02-2017.json";
			//println(base);
			if(i!=2){
				big = true;
			}
			cargarSistema(base);
		}

	}

	// 2017-02-01T09:00:00.000
	@SuppressWarnings("deprecation")
	private Date DateParserString(String fecha){
		try
		{
			return new Date(Integer.parseInt(fecha.substring(0,4)),Integer.parseInt(fecha.substring(5,7)),
					Integer.parseInt(fecha.substring(8,10)));
		}
		catch(IndexOutOfBoundsException e)
		{
			return new Date(0);
		}
	}

}

