package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String taxi_id;
	private String company;
	
	public Taxi(String taxi_id,String company){
		this.taxi_id=taxi_id;
		this.company=company;
	}	
	
	
	public String getTaxi_id() {
		return taxi_id;
	}



	public String getCompany() {
		return company;
	}



	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}	
}