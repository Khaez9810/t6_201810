package model.vo;

import model.data_structures.IList;
import model.data_structures.LinkedList;

public class ZonaServicios implements Comparable<ZonaServicios>{

	private String idZona;
	
	private FechaServicios fechasServicios;
	
	
	public ZonaServicios(String idZona){
		this.idZona=idZona;
		fechasServicios = null;
	}
	
	
	public String getIdZona() {
		return idZona;
	}


	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}


	public FechaServicios getFechasServicios() {
		return fechasServicios;
	}


	public void setFechasServicios(FechaServicios serviciosEnFecha) {
		this.fechasServicios = serviciosEnFecha;
	}
	
	@Override
	public int compareTo(ZonaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
