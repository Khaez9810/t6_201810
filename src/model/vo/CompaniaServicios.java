package model.vo;


import model.data_structures.*;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String nomCompania;
	
	private LinkedList<Servicio> servicios;
	
	public CompaniaServicios(String nomCompania){
		this.nomCompania = nomCompania;
		servicios = new LinkedList<Servicio>();
	}

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public LinkedList<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(LinkedList<Servicio> servicios) {
		this.servicios = servicios;
	}

	public void registerService(Servicio elem){
		if(servicios.size()==0) servicios.add(elem);
		else servicios.add(elem);
	}
	@Override
	public int compareTo(CompaniaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}