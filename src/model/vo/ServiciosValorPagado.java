package model.vo;

import model.data_structures.IList;
import model.data_structures.LinkedList;

public class ServiciosValorPagado {
	
	private LinkedList<Servicio> serviciosAsociados;
	private double valorAcumulado;
	
	public ServiciosValorPagado(){
		serviciosAsociados = new LinkedList<Servicio>();
		valorAcumulado=0;
	}
	
	public long getCantidadservicios(){
		return serviciosAsociados.size();
	}
	
	public LinkedList<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(LinkedList<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	
//	public void registrarServicio(Servicio serv){
//		serviciosAsociados.add(serv);
//		valorAcumulado += serv.getTripTotal();
//	}
	
	public String toString(){
		return "Cantidad de servicios: "+ serviciosAsociados.size()+ " /// " + "Total acomulado: " + valorAcumulado;
	}
	

}
