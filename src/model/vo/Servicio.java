package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	

	private String trip_id;
	private Date trip_start_timestamp;
	private Date trip_end_timestamp;
	private int tripSeconds;
	private Double tripMiles;
	private int Area;

	public Servicio(String trip_id, Date trip_start_timestamp, Date trip_end_timestamp, int tripSeconds, double tripMiles, String Area)
	{
		this.trip_id=trip_id;
		this.trip_start_timestamp=trip_start_timestamp;
		this.trip_end_timestamp=trip_end_timestamp;
		this.tripSeconds=tripSeconds;
		this.tripMiles = tripMiles;
		this.Area = Integer.parseInt(Area);
	}

	public String getTrip_id() {
		return trip_id;
	}
	
	public Date getTrip_start_timestamp()
	{
		return trip_start_timestamp;
	}
	
	public Date getTrip_end_timestamp()
	{
		return trip_end_timestamp;
	}
	
	public int getTripSeconds() {
		return tripSeconds;
	}
	
	public Double getTripMiles()
	{
		return tripMiles;
	}

	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String toString(){
		return "El id del taxi es: " + trip_id + ". La duraci�n del viaje fue: " + tripMiles + ".";
	}
}
