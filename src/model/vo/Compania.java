package model.vo;

import model.data_structures.IList;
import model.data_structures.LinkedList;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private LinkedList<Taxi> taxisInscritos;	
	
	public Compania(String nombre){
		this.nombre = nombre;
		taxisInscritos = new LinkedList<Taxi>();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(LinkedList<Taxi> taxisInscritos) {
		this.taxisInscritos = taxisInscritos;
	}

	public void registerTaxi(Taxi elem){
		if(taxisInscritos.size()==0) taxisInscritos.add(elem);
		else taxisInscritos.add(elem);
	}
	
	public String toString(){
		return "Nombre: " +nombre+" // " + "# de taxis inscritos: " + taxisInscritos.size();
	}
	
	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

}